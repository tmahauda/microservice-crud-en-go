# Microservice CRUD

<div align="center">
<img width="500" height="400" src="golang.jpg">
</div>

## Description du projet

Application web réalisée avec Go en LP MIAR à l'IUT de Nantes dans le cadre du module "Technologies Web coté serveur" durant l'année 2018-2019. \
Elle permet de mettre en place une API REST.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr.

### Encadrants

Ce projet fut encadré par un intervenant profesionnel :
- Laurent GUERIN : lgu.univ@gmail.com.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Technologies Web coté serveur" de la LP MIAR.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2019 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 04/04/2019.
