package utils

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strings"
)

//Récupère la valeur du paramètre d'une requête issue
//de n'importe quelle type de méthode (GET, POST, PUT, DELETE)
func GetParameter(r *http.Request, name string) string {

	var val []string
	var ok bool

	//Si la méthode est "GET", on récupère la valeur dans la QueryString
	//Sinon on recupère la valeur depuis un formulaire
	if r.Method == "GET" {
		val, ok = r.URL.Query()[name]
	} else {
		r.ParseForm()
		val, ok = r.Form[name]
	}

	if ok {
		return val[0]
	} else {
		return ""
	}
}

//Affiche le template de la page HTML dans la réponses avec les données
func DisplayHTML(response http.ResponseWriter, nameTemplate string, data interface{}) bool {

	//Charge le template
	tmpl, errParse := template.ParseFiles("reparties/webapp/src/view/templates/" + nameTemplate + ".gohtml")

	if errParse == nil {
		//Fusionne les données avec le template et les affiches dans la réponse
		errExe := tmpl.ExecuteTemplate(response, nameTemplate+".gohtml", data)

		if errExe == nil {
			return true
		} else {
			DisplayError(response, "Erreur lors de la fusion des données avec la page")
			return false
		}

	} else {
		DisplayError(response, "Erreur lors du chargement de la page")
		return false
	}

}

//Affiche des données JSON dans la réponse
func DisplayJSON(response http.ResponseWriter, data interface{}) bool {
	response.Header().Set("content-type", "application/json;charset=utf-8")
	err := json.NewEncoder(response).Encode(data)
	if err == nil {
		return true
	} else {
		return false
	}
}

//Affiche des données JSON si la requête a été émise directement par le client
//ou la page HTML si la requête a été émise depuis un formulaire
func DisplayHTMLorJSON(response http.ResponseWriter, request *http.Request, url string, data interface{}) {
	//Si la requête a été émise depuis un formulaire, on affiche
	//le résultat sur la page HTML. Sinon on affiche un résultat JSON
	if GetParameter(request, "action") == "" {
		DisplayJSON(response, data)
	} else {
		request.Method = "GET"
		http.Redirect(response, request, url, http.StatusSeeOther)
	}
}

//Affiche une page d'erreur
func DisplayError(response http.ResponseWriter, err string) {
	fmt.Fprint(response, "<html>")
	fmt.Fprint(response, "<head>")
	fmt.Fprint(response, "</head>")
	fmt.Fprint(response, "<body>")
	fmt.Fprintf(response, "<h1>%s</h1>", err)
	fmt.Fprint(response, "</body>")
	fmt.Fprint(response, "</html>")
}

//Recupère l'ID d'un URL en fin de chaîne
func GetId(request *http.Request) string {
	path := request.URL.Path
	pathSplited := strings.Split(path, "/")
	return pathSplited[len(pathSplited)-1]
}
