package entities

import (
	"fmt"
	"strconv"
)

//Une structure qui représente un Student
type Student struct {
	Id           int
	FirstName    string
	LastName     string
	Age          int
	LanguageCode string
}

//Constructeur de Student
func NewStudent(id int, firstName string, lastName string, age int, languageCode string) *Student {
	return &Student{id, firstName, lastName, age, languageCode}
}

//Renvoi un Student sous forme de chaine de caractère
func (this *Student) ToString() string {
	return fmt.Sprintf("Id = %d ; FirstName = %s ; LastName = %s ; Age = %d ; LanguageCode = %s",
		this.Id, this.FirstName, this.LastName, this.Age, this.LanguageCode)
}

func (this *Student) GetId() string {
	return strconv.Itoa(this.Id)
}
