package entities

type Entity interface {
	ToString() string
	GetId() string
}
