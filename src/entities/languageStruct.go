package entities

import (
	"fmt"
)

//Une structure qui représente un Language
type Language struct {
	Code string
	Name string
}

//Constructeur de Language
func NewLanguage(code string, name string) *Language {
	return &Language{code, name}
}

//Renvoi un Language sous forme de chaine de caractère
func (this *Language) ToString() string {
	return fmt.Sprintf("Code = %s ; Name = %s", this.Code, this.Name)
}

func (this *Language) GetId() string {
	return this.Code
}
