package memdb

import (
	"reparties/webapp/src/entities"
	"sync"
)

var studentDataMap DataMap
var studentDataOnce sync.Once

//Recupere une instance unique de la base de données de student
func GetStudentDataMap() *DataMap {
	studentDataOnce.Do(newStudentDataMap)
	return &studentDataMap
}

//Crée une nouvelle base de données de student
func newStudentDataMap() {
	studentDataMap = DataMap{
		map[string]entities.Entity{
			"3": entities.NewStudent(3, "Bart", "Simpsons", 20, "GO"),
			"1": entities.NewStudent(1, "Luke", "Skywalker", 25, "JAVA"),
			"2": entities.NewStudent(2, "Leia", "Organa", 30, "PHP"),
		},
		&sync.Mutex{},
	}
}
