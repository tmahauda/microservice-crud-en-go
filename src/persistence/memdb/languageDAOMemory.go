package memdb

import (
	"net/http"
	"reparties/webapp/src/entities"
	"reparties/webapp/src/utils"
	"sort"
)

//DAO générique
type LanguageDAOMemory struct {
	DAOMemory *DAOMemory
}

//Constructeur de language DAO
func NewLanguageDAOMemory() *LanguageDAOMemory {
	return &LanguageDAOMemory{
		&DAOMemory{
			GetLanguageDataMap(),
		},
	}
}

func (this *LanguageDAOMemory) FindAll() []*entities.Language {
	languages := []*entities.Language{}

	for _, language := range this.DAOMemory.FindAll() {
		languages = append(languages, language.(*entities.Language))
	}

	sort.Slice(languages, func(i, j int) bool {
		return languages[i].Code < languages[j].Code
	})

	return languages
}

func (this *LanguageDAOMemory) Find(code string) *entities.Language {
	language := this.DAOMemory.Find(code)
	if language == nil {
		return nil
	} else {
		return language.(*entities.Language)
	}
}

func (this *LanguageDAOMemory) Exists(code string) bool {
	return this.DAOMemory.Exists(code)
}

func (this *LanguageDAOMemory) Delete(code string) bool {
	return this.DAOMemory.Delete(code)
}

func (this *LanguageDAOMemory) Create(language *entities.Language) bool {
	return this.DAOMemory.Create(language)
}

func (this *LanguageDAOMemory) Update(language *entities.Language) bool {
	return this.DAOMemory.Update(language)
}

func (this *LanguageDAOMemory) Count() int {
	return this.DAOMemory.Count()
}

func (this *LanguageDAOMemory) NewEntity(request *http.Request) *entities.Language {
	code := utils.GetParameter(request, "code")
	name := utils.GetParameter(request, "name")

	if code != "" && name != "" {
		return entities.NewLanguage(code, name)
	} else {
		return nil
	}
}
