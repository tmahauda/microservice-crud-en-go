package memdb

import (
	"net/http"
	"reparties/webapp/src/entities"
	"reparties/webapp/src/utils"
	"sort"
	"strconv"
)

//DAO générique
type StudentDAOMemory struct {
	DAOMemory *DAOMemory
}

//Constructeur de student DAO
func NewStudentDAOMemory() *StudentDAOMemory {
	return &StudentDAOMemory{
		&DAOMemory{
			GetStudentDataMap(),
		},
	}
}

func (this *StudentDAOMemory) FindAll() []*entities.Student {
	students := []*entities.Student{}

	for _, student := range this.DAOMemory.FindAll() {
		students = append(students, student.(*entities.Student))
	}

	sort.Slice(students, func(i, j int) bool {
		return students[i].Id < students[j].Id
	})

	return students
}

func (this *StudentDAOMemory) Find(id string) *entities.Student {
	student := this.DAOMemory.Find(id)
	if student == nil {
		return nil
	} else {
		return student.(*entities.Student)
	}
}

func (this *StudentDAOMemory) Exists(id string) bool {
	return this.DAOMemory.Exists(id)
}

func (this *StudentDAOMemory) Delete(id string) bool {
	return this.DAOMemory.Delete(id)
}

func (this *StudentDAOMemory) Create(student *entities.Student) bool {
	return this.DAOMemory.Create(student)
}

func (this *StudentDAOMemory) Update(student *entities.Student) bool {
	return this.DAOMemory.Update(student)
}

func (this *StudentDAOMemory) Count() int {
	return this.DAOMemory.Count()
}

func (this *StudentDAOMemory) NewEntity(request *http.Request) *entities.Student {
	id, idOK := strconv.Atoi(utils.GetParameter(request, "code"))
	firstName := utils.GetParameter(request, "firstName")
	lastName := utils.GetParameter(request, "lastName")
	age, ageOK := strconv.Atoi(utils.GetParameter(request, "age"))
	language := utils.GetParameter(request, "language")

	if idOK == nil && firstName != "" && lastName != "" && ageOK == nil && age > 0 && language != "" {
		return entities.NewStudent(id, firstName, lastName, age, language)
	} else {
		return nil
	}
}
