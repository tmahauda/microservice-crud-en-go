package memdb

import (
	"reflect"
	"reparties/webapp/src/entities"
)

//DAO générique
type DAOMemory struct {
	Data *DataMap
}

//Retourne le nombre de student dans la base de données
func (this *DAOMemory) Count() int {
	return this.Data.Count()
}

//Retourne une liste de student depuis la base de données
func (this *DAOMemory) FindAll() map[string]entities.Entity {
	return this.Data.Values()
}

//Retourne un student par son id depuis la base de données
func (this *DAOMemory) Find(id string) entities.Entity {
	return this.Data.Read(id)
}

//Vérifie si un student existe dans la base de données par son id
func (this *DAOMemory) Exists(id string) bool {
	return this.Data.Exists(id)
}

//Supprime un student dans la base de données par son id
func (this *DAOMemory) Delete(id string) bool {
	return this.Data.Remove(id)
}

//Ajoute un student dans la base de données
func (this *DAOMemory) Create(entity entities.Entity) bool {
	if reflect.ValueOf(entity).IsNil() || this.Exists(entity.GetId()) {
		return false
	} else {
		return this.Data.Write(entity)
	}
}

//Met à jour un student dans la base de données
func (this *DAOMemory) Update(entity entities.Entity) bool {
	if reflect.ValueOf(entity).IsNil() || !this.Exists(entity.GetId()) {
		return false
	} else {
		return this.Data.Write(entity)
	}
}
