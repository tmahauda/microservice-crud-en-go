package memdb

import (
	"reparties/webapp/src/entities"
	"sync"
)

var languageDataMap DataMap
var languageDataOnce sync.Once

//Recupere une instance unique de la base de données de language
func GetLanguageDataMap() *DataMap {
	languageDataOnce.Do(newLanguageDataMap)
	return &languageDataMap
}

//Crée une nouvelle base de données de language
func newLanguageDataMap() {
	languageDataMap = DataMap{
		map[string]entities.Entity{
			"GO":   entities.NewLanguage("GO", "GoLang"),
			"JAVA": entities.NewLanguage("JAVA", "Java"),
			"PHP":  entities.NewLanguage("PHP", "Php"),
		},
		&sync.Mutex{},
	}
}
