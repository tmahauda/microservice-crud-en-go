package memdb

import (
	"reflect"
	"reparties/webapp/src/entities"
	"sync"
)

//Base de données
type DataMap struct {
	dataMap map[string]entities.Entity
	mux     *sync.Mutex
}

//Fonction qui retourne le nombre de student dans la map
func (this *DataMap) Count() int {
	this.mux.Lock()
	defer this.mux.Unlock()
	count := len(this.dataMap)
	return count
}

//Fonction qui retourne un student par son code s'il existe.
func (this *DataMap) Read(code string) entities.Entity {
	this.mux.Lock()
	defer this.mux.Unlock()
	student, exist := this.dataMap[code]
	if exist {
		return student
	} else {
		return nil
	}
}

//Fonction qui verifie si un student existe dans la base par son code.
func (this *DataMap) Exists(code string) bool {
	this.mux.Lock()
	defer this.mux.Unlock()
	_, exist := this.dataMap[code]
	return exist
}

//Fonction qui ajoute ou modifie un student dans la base
func (this *DataMap) Write(entity entities.Entity) bool {
	this.mux.Lock()
	defer this.mux.Unlock()
	if reflect.ValueOf(entity).IsNil() {
		return false
	} else {
		this.dataMap[entity.GetId()] = entity
		return true
	}
}

//Fonction qui supprime un student dans la base
func (this *DataMap) Remove(code string) bool {
	if this.Exists(code) {
		this.mux.Lock()
		defer this.mux.Unlock()
		delete(this.dataMap, code)
		return true
	}
	return false
}

//Fonction qui récupère les students de la base
func (this *DataMap) Values() map[string]entities.Entity {
	this.mux.Lock()
	defer this.mux.Unlock()
	return this.dataMap
}
