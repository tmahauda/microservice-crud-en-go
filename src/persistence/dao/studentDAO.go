package dao

import (
	"net/http"
	"reparties/webapp/src/entities"
)

type StudentDAO interface {
	FindAll() []*entities.Student
	Find(id string) *entities.Student
	Exists(id string) bool
	Delete(id string) bool
	Create(student *entities.Student) bool
	Update(student *entities.Student) bool
	Count() int
	NewEntity(request *http.Request) *entities.Student
}
