package dao

import (
	"net/http"
	"reparties/webapp/src/entities"
)

type LanguageDAO interface {
	FindAll() []*entities.Language
	Find(code string) *entities.Language
	Exists(code string) bool
	Delete(code string) bool
	Create(language *entities.Language) bool
	Update(language *entities.Language) bool
	Count() int
	NewEntity(request *http.Request) *entities.Language
}
