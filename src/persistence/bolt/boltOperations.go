package bolt

import (
	"github.com/boltdb/bolt"
)

type Bolt struct {
	db *bolt.DB
}

func NewBolt() *Bolt {
	return &Bolt{
		nil,
	}
}

func (this *Bolt) Open(fileName string) bool {
	db, err := bolt.Open(fileName, 0600, nil)
	if err == nil {
		this.db = db
		return true
	} else {
		return false
	}
}

func (this *Bolt) CheckIfOpen() bool {
	return this.db != nil
}

func (this *Bolt) Close() bool {
	if this.CheckIfOpen() {
		err := this.db.Close()
		if err == nil {
			return true
		} else {
			return false
		}
	} else {
		return false
	}
}

func (this *Bolt) Path() string {
	return this.db.Path()
}

// func (this *Bolt) CreateBucket(bucketName string) *bolt.Bucket {
// 	var bucket *bolt.Bucket
// 	var errBuck error

// 	if this.CheckIfOpen() {
// 		err := this.db.Update(func(tx *bolt.Tx) error {

// 			bucket = tx.Bucket([]byte(bucketName))
// 			if bucket == nil {
// 				bucket, errBuck = tx.CreateBucketIfNotExists([]byte(bucketName))
// 				if errBuck != nil {
// 					return errBuck
// 				}
// 				return nil
// 			} else {
// 				return nil
// 			}
// 		})

// 		if err != nil {
// 			return nil
// 		} else {
// 			return bucket
// 		}
// 	} else {
// 		return nil
// 	}
// }

func (this *Bolt) CreateBucket(bucketName string) (*bolt.Bucket, *bolt.Tx) {
	var bucket *bolt.Bucket
	var errBuck error
	if this.CheckIfOpen() {
		tx, errTx := this.db.Begin(true)
		if errTx != nil {
			return nil, nil
		} else {
			bucket = tx.Bucket([]byte(bucketName))
			if bucket == nil {
				bucket, errBuck = tx.CreateBucketIfNotExists([]byte(bucketName))
				if errBuck != nil {
					return nil, nil
				} else {
					return bucket, tx
				}
			} else {
				return bucket, tx
			}
		}
	} else {
		return nil, nil
	}
}

func (this *Bolt) Put(bucketName string, key string, value string) bool {
	bucket, tx := this.CreateBucket(bucketName)
	defer tx.Commit()
	if bucket != nil {
		bucket.Put([]byte(key), []byte(value))
		return true
	} else {
		return false
	}
}

func (this *Bolt) Get(bucketName string, key string) string {
	bucket, tx := this.CreateBucket(bucketName)
	defer tx.Commit()
	if bucket != nil {
		value := string(bucket.Get([]byte(key)))
		return value
	} else {
		return ""
	}
}

func (this *Bolt) GetAll(bucketName string) map[string]string {
	bucket, tx := this.CreateBucket(bucketName)
	defer tx.Commit()
	values := map[string]string{}
	if bucket != nil {
		err := bucket.ForEach(func(key, value []byte) error {
			values[string(key)] = string(value)
			return nil
		})
		if err == nil {
			return values
		} else {
			return nil
		}
	} else {
		return nil
	}
}

func (this *Bolt) Exist(bucket *bolt.Bucket, key string) bool {
	if bucket != nil {
		value := string(bucket.Get([]byte(key)))
		if value == "" {
			return false
		} else {
			return true
		}
	} else {
		return false
	}
}

func (this *Bolt) Delete(bucketName string, key string) bool {
	bucket, tx := this.CreateBucket(bucketName)
	defer tx.Commit()
	if this.Exist(bucket, key) {
		err := bucket.Delete([]byte(key))
		if err == nil {
			return true
		} else {
			return false
		}
	} else {
		return false
	}
}
