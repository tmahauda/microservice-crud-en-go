package main

import (
	"log"
	"net/http"
	"reparties/webapp/src/controllers"
	"reparties/webapp/src/utils"

	"github.com/gorilla/mux"
)

type app struct {
	controllerStudent  *controllers.ControllerStudent
	controllerLanguage *controllers.ControllerLanguage
	router             *mux.Router
}

func newApp() *app {
	app := &app{
		controllers.NewControllerStudent(),
		controllers.NewControllerLanguage(),
		mux.NewRouter(),
	}

	app.initRouter()
	app.initRouterREST()

	return app
}

func main() {
	app := newApp()

	err := http.ListenAndServe(":80", app.run())
	log.Fatal(err)
}

//Filtre les requêtes issues des formulaires de student et language
//pour changer les méthode en PUT ou DELETE d'un formulaire POST selon l'action émise
//En effet HTML5 n'accepte que les méthodes POST ou GET dans les formulaires
func (this *app) run() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//Si la requête est émise depuis un formulaire POST
		if r.Method == "POST" {

			//On récupère l'action du formulaire
			action := utils.GetParameter(r, "action")

			//Si l'action est "update", dans ce cas la méthode est PUT
			//Sinon si l'action est "delete", dans ce cas la méthode est DELETE
			if action == "update" {
				r.Method = "PUT"
			} else if action == "delete" {
				r.Method = "DELETE"
			}
		}
		//On dispatch la requête avec la bonne méthode désirée
		this.router.ServeHTTP(w, r)
	})
}

func (this *app) initRouter() {
	//Student
	this.router.HandleFunc("/student/list", this.controllerStudent.HandlerStudentListData).Methods("GET")
	this.router.HandleFunc("/student/form", this.controllerStudent.HandlerStudentFormData).Methods("GET")

	//Language
	this.router.HandleFunc("/language/list", this.controllerLanguage.HandlerLanguageListData).Methods("GET")
	this.router.HandleFunc("/language/form", this.controllerLanguage.HandlerLanguageFormData).Methods("GET")
}

func (this *app) initRouterREST() {
	//Student
	this.router.HandleFunc("/rest/student", this.controllerStudent.HandlerStudentGetAll).Methods("GET")
	this.router.HandleFunc("/rest/student/{id}", this.controllerStudent.HandlerStudentGetOne).Methods("GET")
	this.router.HandleFunc("/rest/student", this.controllerStudent.HandlerStudentCreate).Methods("POST")
	this.router.HandleFunc("/rest/student", this.controllerStudent.HandlerStudentUpdate).Methods("PUT")
	this.router.HandleFunc("/rest/student/{id}", this.controllerStudent.HandlerStudentDelete).Methods("DELETE")

	//Language
	this.router.HandleFunc("/rest/language", this.controllerLanguage.HandlerLanguageGetAll).Methods("GET")
	this.router.HandleFunc("/rest/language/{id}", this.controllerLanguage.HandlerLanguageGetOne).Methods("GET")
	this.router.HandleFunc("/rest/language", this.controllerLanguage.HandlerLanguageCreate).Methods("POST")
	this.router.HandleFunc("/rest/language", this.controllerLanguage.HandlerLanguageUpdate).Methods("PUT")
	this.router.HandleFunc("/rest/language/{id}", this.controllerLanguage.HandlerLanguageDelete).Methods("DELETE")
}
