package controllers

import (
	"fmt"
	"net/http"
)

//Récupère la liste des languages s'il existe au moins un student à partir d'une requête GET
//	GET /rest/language
func (this *ControllerLanguage) HandlerLanguageGetAll(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Getting all languages")
	handlerGetAll(response, request, this.languageDAOMemory, "/language/list")
}

//Récupère un language par son id s'il existe à partir d'une requête GET
// GET /rest/language/{id}
func (this *ControllerLanguage) HandlerLanguageGetOne(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Getting one language")
	handlerGetOne(response, request, this.languageDAOMemory, "/language/list")
}

//Crée un language à partir d'une requête POST
//	POST /rest/language
func (this *ControllerLanguage) HandlerLanguageCreate(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Creating language")
	handlerCreate(response, request, this.languageDAOMemory, "/language/list")
}

//Met à jour un language par son id s'il existe à partir d'une requête PUT
//	PUT /rest/language
func (this *ControllerLanguage) HandlerLanguageUpdate(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Updating language")
	handlerUpdate(response, request, this.languageDAOMemory, "/language/list")
}

//Supprime un language par son id s'il existe à partir d'une requête DELETE
//	DELETE /rest/language/{id}
func (this *ControllerLanguage) HandlerLanguageDelete(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Deleting language")
	handlerDelete(response, request, this.languageDAOMemory, "/language/list")
}
