package controllers

import (
	"fmt"
	"net/http"
	"reparties/webapp/src/entities"
	"reparties/webapp/src/persistence/memdb"
	"reparties/webapp/src/utils"
)

type ControllerLanguage struct {
	languageDAOMemory *memdb.LanguageDAOMemory
}

func NewControllerLanguage() *ControllerLanguage {
	return &ControllerLanguage{
		memdb.NewLanguageDAOMemory(),
	}
}

//Objet LanguageListData
type LanguageListData struct {
	Languages       []*entities.Language
	NumberLanguages int
}

//Constructeur de LanguageListData
func (this *ControllerLanguage) newLanguageListData(languages []*entities.Language, numberLanguages int) *LanguageListData {
	return &LanguageListData{languages, numberLanguages}
}

//Affiche la liste des languages
func (this *ControllerLanguage) HandlerLanguageListData(response http.ResponseWriter, request *http.Request) {

	fmt.Println("Listing languages")

	//encapsule dans les données à afficher au client la liste et le nombre de student
	data := this.newLanguageListData(this.languageDAOMemory.FindAll(), this.languageDAOMemory.Count())
	utils.DisplayHTML(response, "languageList", data)
}

//Objet LanguageFormData
type LanguageFormData struct {
	Language    *entities.Language
	NewLanguage bool
}

//Constructeur de LanguageFormData
func (this *ControllerLanguage) newLanguageFormData(language *entities.Language, newLanguage bool) *LanguageFormData {
	return &LanguageFormData{language, newLanguage}
}

//Affiche le language dans le formulaire si édition ou
//affiche un nouveau language vierge si création
func (this *ControllerLanguage) HandlerLanguageFormData(response http.ResponseWriter, request *http.Request) {

	fmt.Println("Displaying form language")

	code := utils.GetParameter(request, "code")
	var data *LanguageFormData

	if code == "" {
		data = this.newLanguageFormData(entities.NewLanguage("", ""), true)
	} else {
		data = this.newLanguageFormData(this.languageDAOMemory.Find(code), false)
	}

	utils.DisplayHTML(response, "languageForm", data)
}
