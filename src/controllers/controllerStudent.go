package controllers

import (
	"fmt"
	"net/http"
	"reparties/webapp/src/entities"
	"reparties/webapp/src/persistence/memdb"
	"reparties/webapp/src/utils"
)

type ControllerStudent struct {
	studentDAOMemory *memdb.StudentDAOMemory
}

func NewControllerStudent() *ControllerStudent {
	return &ControllerStudent{
		memdb.NewStudentDAOMemory(),
	}
}

//Objet de type studentListData
type StudentListData struct {
	Students       []*entities.Student
	NumberStudents int
}

//Constructeur de studentListData
func (this *ControllerStudent) newStudentListData(students []*entities.Student, numberStudents int) *StudentListData {
	return &StudentListData{students, numberStudents}
}

//Affiche la liste des students
func (this *ControllerStudent) HandlerStudentListData(response http.ResponseWriter, request *http.Request) {

	fmt.Println("Displaying list student")

	//encapsule dans les données à afficher au client la liste et le nombre de student
	data := this.newStudentListData(this.studentDAOMemory.FindAll(), this.studentDAOMemory.Count())
	utils.DisplayHTML(response, "studentList", data)
}

//Objet StudentFormData
type StudentFormData struct {
	Student    *entities.Student
	NewStudent bool
	Languages  []*entities.Language
}

//Constructeur de StudentFormData
func (this *ControllerStudent) newStudentFormData(student *entities.Student, newStudent bool, languages []*entities.Language) *StudentFormData {
	return &StudentFormData{student, newStudent, languages}
}

//Affiche le student dans le formulaire si édition ou
//affiche un nouveau student vierge si création
func (this *ControllerStudent) HandlerStudentFormData(response http.ResponseWriter, request *http.Request) {

	fmt.Println("Displaying form student")

	languageDAOMemory := memdb.NewLanguageDAOMemory()
	code := utils.GetParameter(request, "code")
	var data *StudentFormData

	if code == "" {
		data = this.newStudentFormData(entities.NewStudent(0, "", "", 0, ""), true, languageDAOMemory.FindAll())
	} else {
		data = this.newStudentFormData(this.studentDAOMemory.Find(code), false, languageDAOMemory.FindAll())
	}

	utils.DisplayHTML(response, "studentForm", data)

}
