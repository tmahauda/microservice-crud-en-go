package controllers

import (
	"fmt"
	"net/http"
	"reflect"
	"reparties/webapp/src/entities"
	"reparties/webapp/src/utils"
)

//Récupère la valeur d'une méthode d'un objet par réfléxivité
func getValue(object interface{}, method string, parameters []reflect.Value) interface{} {
	return reflect.ValueOf(object).MethodByName(method).Call(parameters)[0].Interface()
}

//Récupère la liste des entities s'il existe au moins un entity
func handlerGetAll(response http.ResponseWriter, request *http.Request, DAOMemory interface{}, nextPage string) {

	count := getValue(DAOMemory, "Count", []reflect.Value{}).(int)

	if count == 0 {
		utils.DisplayError(response, fmt.Sprintf("Aucun item à afficher"))
	} else {
		list := getValue(DAOMemory, "FindAll", []reflect.Value{})
		utils.DisplayHTMLorJSON(response, request, nextPage, list)
	}
}

//Récupère un entity par son id s'il existe
func handlerGetOne(response http.ResponseWriter, request *http.Request, DAOMemory interface{}, nextPage string) {

	id := utils.GetId(request)
	entity := getValue(DAOMemory, "Find", []reflect.Value{reflect.ValueOf(id)}).(entities.Entity)

	if reflect.ValueOf(entity).IsNil() {
		utils.DisplayError(response, fmt.Sprintf("L'item numéro %s n'existe pas", id))
	} else {
		utils.DisplayHTMLorJSON(response, request, nextPage, entity)
	}
}

//Crée un entity
func handlerCreate(response http.ResponseWriter, request *http.Request, DAOMemory interface{}, nextPage string) {

	entity := getValue(DAOMemory, "NewEntity", []reflect.Value{reflect.ValueOf(request)}).(entities.Entity)
	create := getValue(DAOMemory, "Create", []reflect.Value{reflect.ValueOf(entity)}).(bool)

	//Si l'étudiant a été crée correctement dans la base de données, on l'affiche
	if create {
		utils.DisplayHTMLorJSON(response, request, nextPage, entity)
	} else {
		utils.DisplayError(response, fmt.Sprintf("L'item n'a pas pu être crée"))
	}
}

//Met à jour un entity s'il existe
func handlerUpdate(response http.ResponseWriter, request *http.Request, DAOMemory interface{}, nextPage string) {

	entity := getValue(DAOMemory, "NewEntity", []reflect.Value{reflect.ValueOf(request)}).(entities.Entity)
	update := getValue(DAOMemory, "Update", []reflect.Value{reflect.ValueOf(entity)}).(bool)

	if update {
		utils.DisplayHTMLorJSON(response, request, nextPage, entity)
	} else {
		utils.DisplayError(response, fmt.Sprintf("L'item numéro %s n'a pas pu être mise à jour", entity.GetId()))
	}
}

//Supprime un entity par son id s'il existe
func handlerDelete(response http.ResponseWriter, request *http.Request, DAOMemory interface{}, nextPage string) {

	fmt.Println("Deleting Student")

	id := utils.GetId(request)
	delete := getValue(DAOMemory, "Delete", []reflect.Value{reflect.ValueOf(id)}).(bool)

	if delete {
		list := getValue(DAOMemory, "FindAll", []reflect.Value{})
		utils.DisplayHTMLorJSON(response, request, nextPage, list)
	} else {
		utils.DisplayError(response, fmt.Sprintf("Le student numéro %s n'a pas pu être supprimé", id))
	}
}
