package controllers

import (
	"fmt"
	"net/http"
)

//Récupère la liste des students s'il existe au moins un student à partir d'une requête GET
//	GET /rest/student
func (this *ControllerStudent) HandlerStudentGetAll(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Getting all students")
	handlerGetAll(response, request, this.studentDAOMemory, "/student/list")
}

//Récupère un student par son id s'il existe à partir d'une requête GET
// GET /rest/student/{id}
func (this *ControllerStudent) HandlerStudentGetOne(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Getting one student")
	handlerGetOne(response, request, this.studentDAOMemory, "/student/list")
}

//Crée un student à partir d'une requête POST
//	POST /rest/student
func (this *ControllerStudent) HandlerStudentCreate(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Creating Student")
	handlerCreate(response, request, this.studentDAOMemory, "/student/list")
}

//Met à jour un student par son id s'il existe à partir d'une requête PUT
//	PUT /rest/student
func (this *ControllerStudent) HandlerStudentUpdate(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Updating Student")
	handlerUpdate(response, request, this.studentDAOMemory, "/student/list")
}

//Supprime un student par son id s'il existe à partir d'une requête DELETE
//	DELETE /rest/student/{id}
func (this *ControllerStudent) HandlerStudentDelete(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Deleting Student")
	handlerDelete(response, request, this.studentDAOMemory, "/student/list")
}
