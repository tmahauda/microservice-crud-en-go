package memdb

import (
	"reparties/webapp/src/persistence/memdb"
	"testing"
)

func TestSingletonStudentDataMap(t *testing.T) {
	studentDataMap1 := memdb.GetStudentDataMap()
	studentDataMap2 := memdb.GetStudentDataMap()

	if studentDataMap1 != studentDataMap2 {
		t.Error("Singleton no respecting")
	}
}
