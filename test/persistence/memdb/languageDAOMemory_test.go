package memdb

import (
	"reparties/webapp/src/entities"
	"reparties/webapp/src/persistence/dao"
	"reparties/webapp/src/persistence/memdb"
	"testing"
)

var languageDAOMemory dao.LanguageDAO = memdb.NewLanguageDAOMemory()

//Test count language in db
func TestCountLanguageDAOMemory(t *testing.T) {
	count := languageDAOMemory.Count()

	if count != 3 {
		t.Errorf("Expected 3 languages, find %d", count)
	}
}

//Test find all language in db
func TestFindAllLanguageDAOMemory(t *testing.T) {
	languages := languageDAOMemory.FindAll()

	if len(languages) != 3 {
		t.Errorf("Expected 3 languages, find %d", len(languages))
	}
}

//Test find language with success
func TestFindOKLanguageDAOMemory(t *testing.T) {
	language := languageDAOMemory.Find("GO")

	if language == nil || language.Code != "GO" {
		t.Errorf("Expected language GO, find nothing")
	}
}

//Test find language with unsuccess
func TestFindKOLanguageDAOMemory(t *testing.T) {
	language := languageDAOMemory.Find("OG")

	if language != nil {
		t.Errorf("Expected nothing, find %s", language.ToString())
	}
}

//Test exist language with success
func TestExistsOKLanguageDAOMemory(t *testing.T) {
	exists := languageDAOMemory.Exists("GO")

	if !exists {
		t.Errorf("Expected true, find false")
	}
}

func TestExistsKOLanguageDAOMemory(t *testing.T) {
	exists := languageDAOMemory.Exists("OGO")

	if exists {
		t.Errorf("Expected false, find true")
	}
}

func TestDeleteOKLanguageDAOMemory(t *testing.T) {
	delete := languageDAOMemory.Delete("GO")

	if !delete {
		t.Errorf("Expected true, find false")
	}
}

func TestDeleteKOLanguageDAOMemory(t *testing.T) {
	delete := languageDAOMemory.Exists("OGOO")

	if delete {
		t.Errorf("Expected false, find true")
	}
}

//Test create with language correct
func TestCreateOKLanguageDAOMemory(t *testing.T) {
	language := entities.NewLanguage("SQL", "Structured Query Language")
	create := languageDAOMemory.Create(language)

	if !create {
		t.Errorf("Expected true, find false")
	}
}

//Test create with language nil
func TestCreateKONilLanguageDAOMemory(t *testing.T) {
	create := languageDAOMemory.Create(nil)

	if create {
		t.Errorf("Expected false, find true")
	}
}

//Test create with language already exist in bd
func TestCreateKOExistLanguageDAOMemory(t *testing.T) {
	language := entities.NewLanguage("GO", "Structured Query Language")
	create := languageDAOMemory.Create(language)

	if !create {
		t.Errorf("Expected false, find true")
	}
}

//Test update with language correct
func TestUpdateOKLanguageDAOMemory(t *testing.T) {
	language := entities.NewLanguage("GO", "LangGO")
	update := languageDAOMemory.Update(language)

	if !update {
		t.Errorf("Expected true, find false")
	}
}

//Test update with language nil
func TestUpdateKONilLanguageDAOMemory(t *testing.T) {
	update := languageDAOMemory.Update(nil)

	if update {
		t.Errorf("Expected false, find true")
	}
}

//Test update with language no exist in bd
func TestUpdateKONoExistLanguageDAOMemory(t *testing.T) {
	language := entities.NewLanguage("OGOOO", "LangGO")
	update := languageDAOMemory.Update(language)

	if update {
		t.Errorf("Expected false, find true")
	}
}
