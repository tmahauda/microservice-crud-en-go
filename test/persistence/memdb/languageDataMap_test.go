package memdb

import (
	"reparties/webapp/src/persistence/memdb"
	"testing"
)

func TestSingletonLanguageDataMap(t *testing.T) {
	languageDataMap1 := memdb.GetLanguageDataMap()
	languageDataMap2 := memdb.GetLanguageDataMap()

	if languageDataMap1 != languageDataMap2 {
		t.Error("Singleton no respecting")
	}
}
