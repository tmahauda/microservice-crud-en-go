package memdb

import (
	"reparties/webapp/src/entities"
	"reparties/webapp/src/persistence/dao"
	"reparties/webapp/src/persistence/memdb"
	"testing"
)

var studentDAOMemory dao.StudentDAO = memdb.NewStudentDAOMemory()

//Test count language in db
func TestCountStudentDAOMemory(t *testing.T) {
	count := studentDAOMemory.Count()

	if count != 3 {
		t.Errorf("Expected 3 students, find %d", count)
	}
}

func TestFindAllStudentDAOMemory(t *testing.T) {
	students := studentDAOMemory.FindAll()

	if len(students) != 3 {
		t.Errorf("Expected 3 languages, find %d", len(students))
	}
}

func TestFindOKStudentDAOMemory(t *testing.T) {
	student := studentDAOMemory.Find("1")

	if student == nil || student.Id != 1 {
		t.Errorf("Expected language GO, find nothing")
	}
}

func TestFindKOStudentDAOMemory(t *testing.T) {
	student := studentDAOMemory.Find("10")

	if student != nil {
		t.Errorf("Expected nothing, find %s", student.ToString())
	}
}

func TestExistsOKStudentDAOMemory(t *testing.T) {
	exists := studentDAOMemory.Exists("1")

	if !exists {
		t.Errorf("Expected true, find false")
	}
}

func TestExistsKOStudentDAOMemory(t *testing.T) {
	exists := studentDAOMemory.Exists("11")

	if exists {
		t.Errorf("Expected false, find true")
	}
}

func TestDeleteOKStudentDAOMemory(t *testing.T) {
	delete := studentDAOMemory.Delete("1")

	if !delete {
		t.Errorf("Expected true, find false")
	}
}

func TestDeleteKOStudentDAOMemory(t *testing.T) {
	delete := studentDAOMemory.Exists("12")

	if delete {
		t.Errorf("Expected false, find true")
	}
}

//Test create with language correct
func TestCreateOKStudentDAOMemory(t *testing.T) {
	student := entities.NewStudent(13, "Théo", "Mahauda", 22, "GO")
	create := studentDAOMemory.Create(student)

	if !create {
		t.Errorf("Expected true, find false")
	}
}

//Test create with language nil
func TestCreateKONilStudentDAOMemory(t *testing.T) {
	create := studentDAOMemory.Create(nil)

	if create {
		t.Errorf("Expected false, find true")
	}
}

//Test create with language already exist in bd
func TestCreateKOExistStudentDAOMemory(t *testing.T) {
	student := entities.NewStudent(1, "Théo", "Mahauda", 22, "GO")
	create := studentDAOMemory.Create(student)

	if !create {
		t.Errorf("Expected false, find true")
	}
}

//Test update with language correct
func TestUpdateOKStudentDAOMemory(t *testing.T) {
	student := entities.NewStudent(1, "Théo", "Mahauda", 22, "GO")
	update := studentDAOMemory.Update(student)

	if !update {
		t.Errorf("Expected true, find false")
	}
}

//Test update with language nil
func TestUpdateKONilStudentDAOMemory(t *testing.T) {
	update := studentDAOMemory.Update(nil)

	if update {
		t.Errorf("Expected false, find true")
	}
}

//Test update with language no exist in bd
func TestUpdateKONoExistStudentDAOMemory(t *testing.T) {
	student := entities.NewStudent(14, "Théo", "Mahauda", 22, "GO")
	update := studentDAOMemory.Update(student)

	if update {
		t.Errorf("Expected false, find true")
	}
}
