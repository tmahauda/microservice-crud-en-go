package bolt

import (
	"reparties/webapp/src/persistence/bolt"
	"testing"
)

func TestOpenBolt(t *testing.T) {
	db := bolt.NewBolt()
	open := db.Open("world.db")

	if !open {
		t.Errorf("Expecting true, find false")
	}
}

//Test with open yes
func TestCheckIfOpenYesBolt(t *testing.T) {
	db := bolt.NewBolt()
	db.Open("world.db")

	if !db.CheckIfOpen() {
		t.Errorf("Expecting true, find false")
	}
}

//Test with open no
func TestCheckIfOpenNoBolt(t *testing.T) {
	db := bolt.NewBolt()

	if db.CheckIfOpen() {
		t.Errorf("Expecting false, find true")
	}
}

//Test with close success
func TestCloseSuccessBolt(t *testing.T) {
	db := bolt.NewBolt()
	db.Open("world.db")

	if !db.Close() {
		t.Errorf("Expecting true, find false")
	}
}

//Test with close failed
func TestCloseFailedBolt(t *testing.T) {
	db := bolt.NewBolt()

	if db.Close() {
		t.Errorf("Expecting false, find true")
	}
}

//Test with create bucket success
func TestCreateBucketSuccessBolt(t *testing.T) {
	db := bolt.NewBolt()
	db.Open("world.db")
	bucket, _ := db.CreateBucket("countries")

	if bucket == nil {
		t.Errorf("Expecting bucket instanciate countries, find nil")
	}
}

//Test with create bucket failed
func TestCreateBucketFailedBolt(t *testing.T) {
	db := bolt.NewBolt()
	bucket, _ := db.CreateBucket("countries")

	if bucket != nil {
		t.Errorf("Expecting nil bucket, find instanciate bucket")
	}
}

//Test with put success in countries
func TestPutBucketSuccessBolt(t *testing.T) {
	db := bolt.NewBolt()
	db.Open("world.db")
	put := db.Put("countries", "FR", "France")

	if !put {
		t.Errorf("Expecting true, find false")
	}
}

//Test with get fr success in countries
func TestGetBucketSuccessBolt(t *testing.T) {
	db := bolt.NewBolt()
	db.Open("world.db")
	db.Put("countries", "FR", "France")
	country := db.Get("countries", "FR")

	if country != "France" {
		t.Errorf("Expecting FR, find %s", country)
	}
}

//Test with get failed with key
func TestGetBucketFailedKeyBolt(t *testing.T) {
	db := bolt.NewBolt()
	db.Open("world.db")
	db.Put("countries", "FR", "France")
	country := db.Get("countries", "RF")

	if country != "" {
		t.Errorf("Expecting nothing, find %s", country)
	}
}

//Test with get failed with buckedName
func TestGetBucketFailedBuckedNameBolt(t *testing.T) {
	db := bolt.NewBolt()
	db.Open("world.db")
	db.Put("countries", "FR", "France")
	country := db.Get("other", "FR")

	if country != "" {
		t.Errorf("Expecting nothing, find %s", country)
	}
}

//Test with getAll success
func TestGetAllBucketSuccessBolt(t *testing.T) {
	db := bolt.NewBolt()
	db.Open("world.db")
	db.Put("countries", "FR", "France")
	db.Put("countries", "JP", "Japan")
	db.Put("countries", "PL", "Poland")

	countries := db.GetAll("countries")

	if len(countries) != 3 {
		t.Errorf("Expecting 3, find %d", len(countries))
	}
}

//Test with delete success
func TestDeleteBucketSuccessBolt(t *testing.T) {
	db := bolt.NewBolt()
	db.Open("world.db")
	db.Put("countries", "FR", "France")
	db.Put("countries", "JP", "Japan")
	db.Put("countries", "PL", "Poland")

	delete := db.Delete("countries", "FR")
	count := len(db.GetAll("countries"))

	if !delete && count != 2 {
		t.Errorf("Expecting true, find false")
	}
}

//Test with delete failed in key
func TestDeleteBucketFailedKeyBolt(t *testing.T) {
	db := bolt.NewBolt()
	db.Open("world.db")
	db.Put("countries", "FR", "France")
	db.Put("countries", "JP", "Japan")
	db.Put("countries", "PL", "Poland")

	delete := db.Delete("countries", "RF")

	if delete {
		t.Errorf("Expecting false, find true")
	}
}
