package utils

import (
	"net/http"
	"net/url"
	"reparties/webapp/src/entities"
	"reparties/webapp/src/utils"
	"strings"
	"testing"
)

type StudentListData struct {
	Students       []*entities.Student
	NumberStudents int
}

//Test GetParameter with GET find parameter
func TestGetParameterGetOKUtil(t *testing.T) {
	request, err := http.NewRequest("GET", "/localhost/student/form", nil)
	if err != nil {
		t.Error(err)
	} else {
		if request.Method != "GET" {
			t.Errorf("Expected method GET, find %s", request.Method)
		} else {
			query := request.URL.Query()
			query.Add("code", "1")
			request.URL.RawQuery = query.Encode()
			code := utils.GetParameter(request, "code")
			if code != "1" {
				t.Errorf("Expected 1, find %s", code)
			}
		}
	}
}

//Test GetParameter GET no find parameter
func TestGetParameterGetKOUtil(t *testing.T) {
	request, err := http.NewRequest("GET", "/localhost/student/form", nil)
	if err != nil {
		t.Error(err)
	} else {
		if request.Method != "GET" {
			t.Errorf("Expected method GET, find %s", request.Method)
		} else {
			code := utils.GetParameter(request, "code")
			if code != "" {
				t.Errorf("Expected nothing, find %s", code)
			}
		}
	}
}

//Test GetParameter with POST find parameter
func TestGetParameterPostOKUtil(t *testing.T) {
	parameter := url.Values{}
	parameter.Set("code", "1")

	request, err := http.NewRequest("POST", "/localhost/student/form", strings.NewReader(parameter.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")

	if err != nil {
		t.Error(err)
	} else {
		if request.Method != "POST" {
			t.Errorf("Expected method POST, find %s", request.Method)
		} else {
			code := utils.GetParameter(request, "code")
			if code != "1" {
				t.Errorf("Expected 1, find %s", code)
			}
		}
	}
}

//Test GetParameter with POST no find parameter
func TestGetParameterPostKOUtil(t *testing.T) {
	parameter := url.Values{}

	request, err := http.NewRequest("POST", "/localhost/student/form", strings.NewReader(parameter.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")

	if err != nil {
		t.Error(err)
	} else {
		if request.Method != "POST" {
			t.Errorf("Expected method POST, find %s", request.Method)
		} else {
			code := utils.GetParameter(request, "code")
			if code != "" {
				t.Errorf("Expected nothing, find %s", code)
			}
		}
	}
}

//Test display html ok
func TestDisplayHTMLOKUtil(t *testing.T) {

	http.HandleFunc("/localhost/studentTest/list", func(response http.ResponseWriter, request *http.Request) {
		data := StudentListData{[]*entities.Student{}, 0}
		display := utils.DisplayHTML(response, "studentList", data)

		if !display {
			t.Errorf("Expected true, find false")
		}
	})

	request, err := http.NewRequest("GET", "/localhost/studentTest/list", nil)

	if err != nil {
		t.Error(err)
	} else {
		client := &http.Client{}
		client.Do(request)
	}
}

//Test display html with template no existing
func TestDisplayHTMLKOTemplateNoExistingUtil(t *testing.T) {

	http.HandleFunc("/localhost/studentTest/list", func(response http.ResponseWriter, request *http.Request) {
		data := StudentListData{[]*entities.Student{}, 0}
		display := utils.DisplayHTML(response, "studentNoExisting", data)

		if display {
			t.Errorf("Expected false, find true")
		}
	})

	request, err := http.NewRequest("GET", "/localhost/studentTest/list", nil)

	if err != nil {
		t.Error(err)
	} else {
		client := &http.Client{}
		client.Do(request)
	}
}

//Test display html with data nil
func TestDisplayHTMLKODataNilUtil(t *testing.T) {

	http.HandleFunc("/localhost/studentTest/list", func(response http.ResponseWriter, request *http.Request) {
		display := utils.DisplayHTML(response, "studentList", nil)

		if display {
			t.Errorf("Expected false, find true")
		}
	})

	request, err := http.NewRequest("GET", "/localhost/studentTest/list", nil)

	if err != nil {
		t.Error(err)
	} else {
		client := &http.Client{}
		client.Do(request)
	}
}

//Test display html with data bad instanciate
func TestDisplayHTMLKODataBadInstanciateUtil(t *testing.T) {

	http.HandleFunc("/localhost/studentTest/list", func(response http.ResponseWriter, request *http.Request) {
		data := StudentListData{nil, 0}
		display := utils.DisplayHTML(response, "studentList", data)

		if display {
			t.Errorf("Expected false, find true")
		}
	})

	request, err := http.NewRequest("GET", "/localhost/studentTest/list", nil)

	if err != nil {
		t.Error(err)
	} else {
		client := &http.Client{}
		client.Do(request)
	}
}

//Test display JSON ok
func TestDisplayJSONOKUtil(t *testing.T) {

	http.HandleFunc("/localhost/studentTest/list", func(response http.ResponseWriter, request *http.Request) {
		display := utils.DisplayJSON(response, []*entities.Student{})

		if !display {
			t.Errorf("Expected true, find false")
		}
	})

	request, err := http.NewRequest("GET", "/localhost/studentTest/list", nil)

	if err != nil {
		t.Error(err)
	} else {
		client := &http.Client{}
		client.Do(request)
	}
}

//Test display JSON with data nil
func TestDisplayJSONKODataNilUtil(t *testing.T) {

	http.HandleFunc("/localhost/studentTest/list", func(response http.ResponseWriter, request *http.Request) {
		display := utils.DisplayJSON(response, nil)

		if display {
			t.Errorf("Expected false, find true")
		}
	})

	request, err := http.NewRequest("GET", "/localhost/studentTest/list", nil)

	if err != nil {
		t.Error(err)
	} else {
		client := &http.Client{}
		client.Do(request)
	}
}

func TestGetId(t *testing.T) {
	request, err := http.NewRequest("GET", "/localhost/studentTest/list/GO", nil)

	if err != nil {
		t.Error(err)
	} else {
		id := utils.GetId(request)

		if id != "GO" {
			t.Errorf("Expected GO, find %s", id)
		}
	}
}
